# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-25 00:42+0000\n"
"PO-Revision-Date: 2018-06-09 20:17+0300\n"
"Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 2.0\n"

#: kio_tags.cpp:110 kio_tags.cpp:166
#, kde-format
msgid "File %1 already has tag %2"
msgstr "للملف %1 الوسم %2 بالفعل"

#: kio_tags.cpp:298
#, kde-format
msgid "Tag"
msgstr "وسم"

#: kio_tags.cpp:304
#, kde-format
msgid "Tag Fragment"
msgstr "جزء من وسم"

#: kio_tags.cpp:316 kio_tags.cpp:317
#, kde-format
msgid "All Tags"
msgstr "كل الوسوم"
